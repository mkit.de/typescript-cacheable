/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable unused-imports/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import EventEmitter from 'events'
import { MapEntry } from './MapEntry'

export class ExpiringMap<K, V> extends EventEmitter {
    private store: Map<K, MapEntry<V>>

    constructor() {
        super()
        this.store = new Map<K, MapEntry<V>>()
        this.on('save', (_args: any[]) => {
            this.clean()
        })
    }

    public set(key: K, value: V, duration?: number): void {
        const entity = new MapEntry(value, duration)
        this.store.set(key, entity)
        this.emit('save', key, value, duration)
    }

    public get(key: K): any {
        const entity = this.store.get(key)
        if (entity === undefined) {
            return undefined
        }
        if (entity.isExpired) {
            this.store.delete(key)
            return undefined
        }
        return entity.data
    }

    public has(key: K): boolean {
        return this.store.has(key) && !this.store.get(key)?.isExpired
    }

    public delete(key: K): void {
        if (this.store.has(key)) {
            this.store.delete(key)
        }
    }

    public clear(): void {
        this.store.clear()
    }

    public get size(): number {
        return this.store.size
    }

    public keys(): K[] {
        return Array.from(this.store.keys())
    }

    private clean(): void {
        this.store.forEach((value: MapEntry<V>, key: K) => {
            if (value.isExpired) {
                this.store.delete(key)
            }
        })
    }
}
